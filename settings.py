from typing import Optional

from dotenv import load_dotenv
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    debug: bool = False

    api_host: str = "0.0.0.0"
    api_port: int = 5000
    git_commit_hash: Optional[str] = None
    git_commit_branch: Optional[str] = None

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
