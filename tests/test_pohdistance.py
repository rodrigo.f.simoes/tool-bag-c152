import itertools
import unittest

from src.models import POHDistanceTable
from src.pohdistance import poh_distance


class TestPOHDistanceCalculator(unittest.TestCase):
    c152_takeoff_distances = POHDistanceTable(
        temperatures=[0, 10, 20, 30, 40],
        pressure_altitude=[0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000],
        grass_factor=0.15,
        wind_correction=lambda head_wind: (
            1 - (0.1 * abs(head_wind / 9))
            if head_wind > 0
            else 1 + (0.1 * abs(head_wind / 2))
        ),
        ground_roll=[
            (640, 695, 755, 810, 875),
            (705, 765, 825, 890, 960),
            (775, 840, 910, 980, 1055),
            (855, 925, 1000, 1080, 1165),
            (940, 1020, 1100, 1190, 1285),
            (1040, 1125, 1215, 1315, 1420),
            (1145, 1245, 1345, 1455, 1570),
            (1270, 1375, 1490, 1615, 1745),
            (1405, 1525, 1655, 1795, 1940),
        ],
        screen_height=[
            (1190, 1290, 1390, 1495, 1605),
            (1310, 1420, 1530, 1645, 1770),
            (1445, 1565, 1690, 1820, 1960),
            (1600, 1730, 1870, 2020, 2185),
            (1775, 1920, 2080, 2250, 2440),
            (1970, 2140, 2320, 2525, 2750),
            (2200, 2395, 2610, 2855, 3125),
            (2470, 2705, 2960, 3255, 3590),
            (2800, 3080, 3395, 3765, 4195),
        ],
    )

    def test_sample_on_poh(self):
        """POH 5-4"""
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                pressure_altitude=2000,
                temperature=30,
                head_wind=12,
            ).dict(),
            {"ground_roll": 849, "screen_height": 1577},
        )

    def test_lower_temp_than_table(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                temperature=-10,
            ).dict(),
            {"ground_roll": 640, "screen_height": 1190},
        )

    def test_higher_temp_than_table(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                temperature=45,
            ).dict(),
            {"ground_roll": 875, "screen_height": 1605},
        )

    def test_higher_altitude_than_table(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                pressure_altitude=9000,
            ).dict(),
            {"ground_roll": 1405, "screen_height": 2800},
        )

    def test_lower_altitude_than_table(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                pressure_altitude=-150,
            ).dict(),
            {"ground_roll": 640, "screen_height": 1190},
        )

    def test_no_headwind(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=0,
            ).dict(),
            {"ground_roll": 640, "screen_height": 1190},
        )

    def test_headwind_9(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=9,
            ).dict(),
            {"ground_roll": 576, "screen_height": 1071},
        )

    def test_headwind_18(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=18,
            ).dict(),
            {"ground_roll": 512, "screen_height": 952},
        )

    def test_headwind_27(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=27,
            ).dict(),
            {"ground_roll": 448, "screen_height": 833},
        )

    def test_headwind_36(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=36,
            ).dict(),
            {"ground_roll": 384, "screen_height": 714},
        )

    def test_tailwind_2(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=-2,
            ).dict(),
            {"ground_roll": 704, "screen_height": 1309},
        )

    def test_tailwind_4(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=-4,
            ).dict(),
            {"ground_roll": 768, "screen_height": 1428},
        )

    def test_tailwind_6(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=-6,
            ).dict(),
            {"ground_roll": 832, "screen_height": 1547},
        )

    def test_tailwind_8(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=-8,
            ).dict(),
            {"ground_roll": 896, "screen_height": 1666},
        )

    def test_tailwind_10(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                head_wind=-10,
            ).dict(),
            {"ground_roll": 960, "screen_height": 1785},
        )

    def test_can_reach_all_values(self):
        self.assertIsNotNone(
            list(
                itertools.product(
                    self.c152_takeoff_distances.pressure_altitude,
                    self.c152_takeoff_distances.temperatures,
                )
            )
        )

    def test_grass(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                grass=True,
            ).dict(),
            {"ground_roll": 736, "screen_height": 1190},
        )

    def test_grass_and_wind(self):
        self.assertEqual(
            poh_distance(
                poh_values=self.c152_takeoff_distances,
                grass=True,
                head_wind=9,
            ).dict(),
            {"ground_roll": 662, "screen_height": 1071},
        )


if __name__ == "__main__":
    unittest.main()
