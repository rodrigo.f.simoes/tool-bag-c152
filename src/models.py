from typing import Callable

from pydantic import BaseModel, model_validator


class POHDistanceTable(BaseModel):
    temperatures: list[int]
    pressure_altitude: list[int]
    ground_roll: list[tuple]
    screen_height: list[tuple]
    grass_factor: float
    wind_correction: Callable


class POHRateOfClimb(BaseModel):
    temperatures: list[int]
    pressure_altitude: list[int]
    climb_speed: list[int]
    rate_of_climb: list[tuple]


class POHTimeFuelDistanceToClimb(BaseModel):
    pressure_altitude: list[int]
    temperatures: list[int]
    climb_speed: list[int]
    rate_of_climb: list[int]
    time: list[int]
    fuel_used: list[float]
    distance: list[int]
    temperature_correction: Callable


class POHCruisePerformance(BaseModel):
    pressure_altitude: list[int]
    isa_deviation: list[int]
    rpm: list[int]
    fairings_correction: int
    brake_horse_power: list[
        list[tuple]
    ]  # 1 está pressure alt (2000~12000) 2 rpm (2550 ~ 2000) 3 isa dev (-20, 0, 20)
    true_air_speed: list[list[tuple]]
    fuel_used: list[list[tuple]]
