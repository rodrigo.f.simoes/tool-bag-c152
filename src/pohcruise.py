import numpy as np
from pydantic import BaseModel
from scipy.interpolate import RegularGridInterpolator

from src.isa import isa_model
from src.models import POHCruisePerformance


class POHCruiseResponse(BaseModel):
    brake_horse_power: int
    true_air_speed: int
    fuel_used: float


def poh_cruise(
        poh_values: POHCruisePerformance,
        pressure_altitude: int = 0,
        temperature: int = 15,
        rpm: int = 0,
        fairings_installed: bool = False,
) -> POHCruiseResponse:
    """UNSTABLE due to non-linear axis of cruise performance table USE WITH CAUTION!"""
    isa_deviation = int(temperature - isa_model(pressure_altitude).temperature)
    isa_deviation = max(
        min(isa_deviation, poh_values.isa_deviation[-1]),
        poh_values.isa_deviation[0],
    )

    pressure_altitude = max(
        min(pressure_altitude, poh_values.pressure_altitude[-1]),
        poh_values.pressure_altitude[0],
    )

    rpm = max(
        min(rpm, poh_values.rpm[0]),
        poh_values.rpm[-1],
    )

    brake_horse_power_interpolation = RegularGridInterpolator(
        (poh_values.isa_deviation, poh_values.rpm, poh_values.pressure_altitude),
        np.array(poh_values.brake_horse_power).T,
    )
    true_air_speed_interpolation = RegularGridInterpolator(
        (poh_values.isa_deviation, poh_values.rpm, poh_values.pressure_altitude),
        np.array(poh_values.true_air_speed).T,
    )
    fuel_used_interpolation = RegularGridInterpolator(
        (poh_values.isa_deviation, poh_values.rpm, poh_values.pressure_altitude),
        np.array(poh_values.fuel_used).T,
    )

    brake_horse_power = float(
        brake_horse_power_interpolation((isa_deviation, rpm, pressure_altitude))
    )
    true_air_speed = float(
        true_air_speed_interpolation((isa_deviation, rpm, pressure_altitude))
    )
    fuel_used = float(fuel_used_interpolation((isa_deviation, rpm, pressure_altitude)))

    fairings_correction = poh_values.fairings_correction if fairings_installed else 0

    if brake_horse_power < 0 or true_air_speed < 0 or fuel_used < 0:
        raise Exception("Requested arguments outside poh given data")

    return POHCruiseResponse(
        brake_horse_power=int(brake_horse_power),
        true_air_speed=int(true_air_speed + fairings_correction),
        fuel_used=float(fuel_used),
    )
