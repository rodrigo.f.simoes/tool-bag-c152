from pydantic import BaseModel
from scipy.interpolate import interp1d

from src.models import POHTimeFuelDistanceToClimb


class POHTimeFuelDistanceToClimbResponse(BaseModel):
    climb_speed: int
    rate_of_climb: int
    time: float
    fuel_used: float
    distance: float


def poh_time_fuel_distance_to_climb(
    poh_values: POHTimeFuelDistanceToClimb,
    pressure_altitude: int = 0,
    temperature: int = 0,
) -> POHTimeFuelDistanceToClimbResponse:
    pressure_altitude = max(
        min(pressure_altitude, poh_values.pressure_altitude[-1]),
        poh_values.pressure_altitude[0],
    )

    isa_temperature_interpolation = interp1d(
        poh_values.pressure_altitude, poh_values.temperatures
    )
    climb_speed_interpolation = interp1d(
        poh_values.pressure_altitude, poh_values.climb_speed
    )
    rate_of_climb_interpolation = interp1d(
        poh_values.pressure_altitude, poh_values.rate_of_climb
    )
    time_interpolation = interp1d(poh_values.pressure_altitude, poh_values.time)
    fuel_used_interpolation = interp1d(
        poh_values.pressure_altitude, poh_values.fuel_used
    )
    distance_interpolation = interp1d(poh_values.pressure_altitude, poh_values.distance)

    isa_temperature = int(isa_temperature_interpolation(pressure_altitude))

    return POHTimeFuelDistanceToClimbResponse(
        climb_speed=int(climb_speed_interpolation(pressure_altitude)),
        rate_of_climb=int(rate_of_climb_interpolation(pressure_altitude)),
        time=float(time_interpolation(pressure_altitude))
        * poh_values.temperature_correction(isa_temperature, temperature),
        fuel_used=float(fuel_used_interpolation(pressure_altitude))
        * poh_values.temperature_correction(isa_temperature, temperature),
        distance=float(distance_interpolation(pressure_altitude))
        * poh_values.temperature_correction(isa_temperature, temperature),
    )
