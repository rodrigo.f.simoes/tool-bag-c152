from fastapi import APIRouter
from pydantic import BaseModel

from src.c152 import c152_takeoff_distances, c152_landing_distances, c152_rate_of_climb, \
    c152_time_fuel_distance_to_climb, c152_cruise_performance
from src.pohcruise import POHCruiseResponse, poh_cruise
from src.pohdistance import POHDistanceResponse, poh_distance
from src.pohrateofclimb import POHRateOfClimbResponse, poh_rate_of_climb
from src.pohtimefueldistanceclimb import POHTimeFuelDistanceToClimbResponse, poh_time_fuel_distance_to_climb

c152_api = APIRouter(prefix="/c152")


class DistanceVariables(BaseModel):
    pressure_altitude: int = 0
    temperature: int = 0
    head_wind: float = 0.0
    grass: bool = False
    short_field: bool = True


@c152_api.post("/takeoff/distance", response_model=POHDistanceResponse)
def takeoff_distance(variables: DistanceVariables):
    return poh_distance(
        poh_values=c152_takeoff_distances,
        pressure_altitude=variables.pressure_altitude,
        temperature=variables.temperature,
        head_wind=variables.head_wind,
        grass=variables.grass,
        short_field=variables.short_field
    )


@c152_api.post("/landing/distance", response_model=POHDistanceResponse)
def landing_distance(variables: DistanceVariables):
    return poh_distance(
        poh_values=c152_landing_distances,
        pressure_altitude=variables.pressure_altitude,
        temperature=variables.temperature,
        head_wind=variables.head_wind,
        grass=variables.grass,
        short_field=variables.short_field
    )


class AirportVariables(BaseModel):
    pressure_altitude: int = 0
    temperature: int = 0


@c152_api.post("/climb/rate", response_model=POHRateOfClimbResponse)
def rate_of_climb(variables: AirportVariables):
    return poh_rate_of_climb(
        poh_values=c152_rate_of_climb,
        pressure_altitude=variables.pressure_altitude,
        temperature=variables.temperature,
    )


@c152_api.post("/climb/timefueldistance", response_model=POHTimeFuelDistanceToClimbResponse)
def time_fuel_distance_to_climb(variables: AirportVariables):
    return poh_time_fuel_distance_to_climb(
        poh_values=c152_time_fuel_distance_to_climb,
        pressure_altitude=variables.pressure_altitude,
        temperature=variables.temperature,
    )


class CruiseVariables(BaseModel):
    pressure_altitude: int = 0
    temperature: int = 15
    rpm: int = 0
    fairings_installed: bool = False


@c152_api.post("/cruise", response_model=POHCruiseResponse | dict)
def cruise(variables: CruiseVariables):
    try:
        return poh_cruise(
            poh_values=c152_cruise_performance,
            temperature=variables.temperature,
            rpm=variables.rpm,
            pressure_altitude=variables.pressure_altitude,
            fairings_installed=variables.fairings_installed
        )
    except:
        return {"error": "Variables outside of allowed values. Check poh values."}
