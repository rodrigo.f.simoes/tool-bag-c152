from fastapi import APIRouter

from src.api.v1.c152 import c152_api

v1_api = APIRouter(prefix="/v1")
v1_api.include_router(c152_api)


