import numpy as np
from pydantic import BaseModel
from scipy.interpolate import RegularGridInterpolator

from src.models import POHDistanceTable


class POHDistanceResponse(BaseModel):
    ground_roll: int
    screen_height: int


def poh_distance(
        poh_values: POHDistanceTable,
        pressure_altitude: int = 0,
        temperature: int = 0,
        head_wind: float = 0.0,
        grass: bool = False,
        short_field: bool = True,
) -> POHDistanceResponse:
    temperature = max(
        min(temperature, poh_values.temperatures[-1]),
        poh_values.temperatures[0],
    )

    pressure_altitude = max(
        min(pressure_altitude, poh_values.pressure_altitude[-1]),
        poh_values.pressure_altitude[0],
    )
    pressure_altitude = pressure_altitude if short_field else pressure_altitude + 1000

    ground_roll_interpolation = RegularGridInterpolator(
        (poh_values.temperatures, poh_values.pressure_altitude),
        np.array(poh_values.ground_roll).T,
    )
    screen_height_interpolation = RegularGridInterpolator(
        (poh_values.temperatures, poh_values.pressure_altitude),
        np.array(poh_values.screen_height).T,
    )

    ground_roll = float(ground_roll_interpolation((temperature, pressure_altitude)))
    screen_height = float(screen_height_interpolation((temperature, pressure_altitude)))

    grass_factor = 1 + poh_values.grass_factor if grass else 1
    distance_factor = poh_values.wind_correction(head_wind)

    return POHDistanceResponse(
        ground_roll=int(ground_roll * distance_factor * grass_factor),
        screen_height=int(screen_height * distance_factor),
    )
