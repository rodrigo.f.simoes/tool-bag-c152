import numpy as np
from pydantic import BaseModel
from scipy.interpolate import RegularGridInterpolator, interp1d

from src.models import POHRateOfClimb


class POHRateOfClimbResponse(BaseModel):
    climb_speed: int
    rate_of_climb: int


def poh_rate_of_climb(
    poh_values: POHRateOfClimb,
    pressure_altitude: int = 0,
    temperature: int = 0,
) -> POHRateOfClimbResponse:
    temperature = max(
        min(temperature, poh_values.temperatures[-1]),
        poh_values.temperatures[0],
    )

    pressure_altitude = max(
        min(pressure_altitude, poh_values.pressure_altitude[-1]),
        poh_values.pressure_altitude[0],
    )

    climb_speed_interpolation = interp1d(
        poh_values.pressure_altitude, poh_values.climb_speed
    )

    rate_of_climb_interpolation = RegularGridInterpolator(
        (poh_values.temperatures, poh_values.pressure_altitude),
        np.array(poh_values.rate_of_climb).T,
    )

    rate_of_climb = float(rate_of_climb_interpolation((temperature, pressure_altitude)))

    return POHRateOfClimbResponse(
        climb_speed=int(climb_speed_interpolation(pressure_altitude)),
        rate_of_climb=int(rate_of_climb),
    )
