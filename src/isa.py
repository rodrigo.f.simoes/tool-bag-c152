import math

from pydantic import BaseModel

GRAVITY_ACCELERATION = 9.80665  # Acceleration due to gravity in m/s^2
SPECIFIC_GAS_CONSTANT = 287.00  # Specific gas constant for dry air in J/(kg*K)


def calculate_conditions(initial_pressure, initial_temperature, lapse_rate, initial_altitude, target_altitude):
    if lapse_rate != 0:
        target_temperature = initial_temperature + lapse_rate * (target_altitude - initial_altitude)
        target_pressure = initial_pressure * (target_temperature / initial_temperature) ** (
                -GRAVITY_ACCELERATION / lapse_rate / SPECIFIC_GAS_CONSTANT)
    else:
        target_temperature = initial_temperature
        target_pressure = initial_pressure * math.exp(
            -GRAVITY_ACCELERATION / SPECIFIC_GAS_CONSTANT / initial_temperature * (target_altitude - initial_altitude))
    return target_temperature, target_pressure


class ISA(BaseModel):
    temperature: float  # Celcius
    pressure: float
    air_density: float


def isa_model(altitude: int) -> ISA:
    lapse_rates = [-0.0065, 0, 0.001, 0.0028]
    altitude_layers = [11000, 20000, 32000, 47000]
    sea_level_pressure = 101325
    sea_level_temperature = 288.15
    previous_layer_altitude = 0

    if altitude < 0 or altitude > 47000:
        print("Altitude must be in the range [0, 47000] meters.")
        return

    for i in range(len(lapse_rates)):
        if altitude <= altitude_layers[i]:
            temperature, pressure = calculate_conditions(sea_level_pressure, sea_level_temperature, lapse_rates[i],
                                                         previous_layer_altitude, altitude)
            break
        else:
            sea_level_temperature, sea_level_pressure = calculate_conditions(sea_level_pressure, sea_level_temperature,
                                                                             lapse_rates[i], previous_layer_altitude,
                                                                             altitude_layers[i])
            previous_layer_altitude = altitude_layers[i]

    air_density = pressure / (SPECIFIC_GAS_CONSTANT * temperature)
    return ISA(temperature=temperature - 273.15, pressure=pressure, air_density=air_density)
