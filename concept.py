from src.c152 import (
    c152_takeoff_distances,
    c152_landing_distances,
    c152_rate_of_climb,
    c152_time_fuel_distance_to_climb,
    c152_cruise_performance,
)
from src.pohcruise import poh_cruise
from src.pohdistance import poh_distance
from src.pohrateofclimb import poh_rate_of_climb
from src.pohtimefueldistanceclimb import poh_time_fuel_distance_to_climb

def main():
    pressure_alt = 326
    temp = 28
    rpm = 2300
    head_wind = 0

    print(
        "TAKE OFF DISTANCE\t",
        poh_distance(
            poh_values=c152_takeoff_distances,
            pressure_altitude=pressure_alt,
            temperature=temp,
            head_wind=head_wind,
        ),
        "\nLANDING DISTANCE\t",
        poh_distance(
            poh_values=c152_landing_distances,
            pressure_altitude=pressure_alt,
            temperature=temp,
            head_wind=head_wind,
        ),
    )

    print(
        "RATE OF CLIMB\t",
        poh_rate_of_climb(
            poh_values=c152_rate_of_climb,
            pressure_altitude=pressure_alt,
            temperature=temp,
        ),
    )

    print(
        "TIME, FUEL, AND DISTANCE TO CLIMB\t",
        poh_time_fuel_distance_to_climb(
            poh_values=c152_time_fuel_distance_to_climb,
            pressure_altitude=pressure_alt,
            temperature=temp,
        ),
    )

    print(
        "CRUISE PERFORMANCE\t",
        poh_cruise(
            poh_values=c152_cruise_performance,
            temperature=temp,
            rpm=rpm,
            pressure_altitude=pressure_alt,
        ),
    )


if __name__ == "__main__":
    main()
